package ru.trifonov.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.trifonov.tm.api.IProjectRepository;
import ru.trifonov.tm.entity.*;

import java.util.*;

public final class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {
    @Override
    public void persist(@NotNull final Project project) {
        entities.put(project.getId(), project);
    }

    @Override
    public void merge(@NotNull final Project project) {
        if (entities.containsKey(project.getId())) {
            update(project.getName(), project.getId(), project.getUserId(), project.getDescription(), project.getBeginDate(), project.getEndDate());
        } else
            insert(project.getName(), project.getId(), project.getUserId(), project.getDescription(), project.getBeginDate(), project.getEndDate());
    }

    @Override
    public void insert(@NotNull final String name, @NotNull final String id, @NotNull final String userId, @NotNull final String description, @NotNull final Date beginDate, @NotNull final Date endDate) {
        @NotNull final Project project = new Project(name, id, userId, description, beginDate, endDate);
        entities.put(id, project);
    }

    @Override
    public void update(@NotNull final String name, @NotNull final String id, @NotNull final String userId, @NotNull final String description, @NotNull final Date beginDate, @NotNull final Date endDate) {
        @NotNull final Project project = new Project(name, id, userId, description, beginDate, endDate);
        entities.put(id, project);
    }

    @Override
    public Project findOne(@NotNull final String id, @NotNull final String userId) {
        @NotNull Project project = entities.get(id);
        if (!project.getUserId().equals(userId)) throw new IllegalStateException();
        return project;
    }

    @Override
    public Collection<Project> findAll(@NotNull final String userId) {
        @NotNull final Collection<Project> output = new ArrayList<>();
        for (@NotNull Map.Entry<String, Project> project : entities.entrySet()) {
            if (project.getValue().getUserId().equals(userId)){
                output.add(project.getValue());
            }
            if (output.isEmpty()) throw new NullPointerException();
        }
        return output;
    }


    @Override
    public void remove(@NotNull final String id, @NotNull final String userId) {
        @NotNull final Iterator<Map.Entry<String, Project>> entryIterator = entities.entrySet().iterator();
        while (entryIterator.hasNext()) {
            Map.Entry<String, Project> projectEntry = entryIterator.next();
            if (projectEntry.getKey().equals(id) && projectEntry.getValue().getUserId().equals(userId)) {
                entryIterator.remove();
                break;
            }
        }
    }

    @Override
    public void removeAll(@NotNull final String userId) {
        @NotNull final Iterator<Map.Entry<String, Project>> entryIterator = entities.entrySet().iterator();
        while (entryIterator.hasNext()) {
            Map.Entry<String, Project> projectEntry = entryIterator.next();
            if (projectEntry.getValue().getUserId().equals(userId)) {
                entryIterator.remove();
            }
        }
    }
}