package ru.trifonov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.api.IUserRepository;
import ru.trifonov.tm.entity.User;
import ru.trifonov.tm.enumerate.RoleType;

import java.util.*;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {
    @Override
    public void persist(@NotNull final User user) {
        entities.put(user.getId(), user);
    }

    @Override
    public void merge(@NotNull final User user) {
        if (entities.containsKey(user.getId())) {
            update(user.getId(), user.getLogin(), user.getPasswordMD5(), user.getRoleType());
        } else {
            insert(user.getId(), user.getLogin(), user.getPasswordMD5(), user.getRoleType());
        }
    }

    @Override
    public void insert(@NotNull final String id, @NotNull final String login, @NotNull final String passwordMD5, @NotNull final RoleType roleType) {
        @NotNull final User user = new User(id, login, passwordMD5, roleType);
        entities.put(id, user);
    }

    @Override
    public void update(@NotNull final String id, @NotNull final String login, @NotNull final String passwordMD5, @NotNull final RoleType roleType) {
        @NotNull final User user = new User(id, login, passwordMD5, roleType);
        entities.put(id, user);
    }

    @Override
    public User findOne(@NotNull final String id) {
        @NotNull final User user = entities.get(id);
        return user;
    }

    @Override
    public Collection<User> findAll() {
        @NotNull final Collection<User> output = new ArrayList<>();
        for (@NotNull Map.Entry<String, User> user : entities.entrySet()) {
            output.add(user.getValue());
        }
        if (output.isEmpty()) throw new NullPointerException();
        return output;
    }

    @Override
    public User findLogin(@NotNull final String login) {
        @Nullable User foundUser = null;
        for (User user : entities.values()) {
            if (user.getLogin().equals(login)) foundUser = user;
        }
        return foundUser;
    }

    @Override
    public User findPassword(@NotNull final User user, @NotNull final String PasswordMD5) {
        if (!user.getPasswordMD5().equals(PasswordMD5)) throw new IllegalStateException();
        return user;
    }

    @Override
    public void removeOne(@NotNull final String id) {
        final Iterator<Map.Entry<String, User>> entryIterator= entities.entrySet().iterator();
        while (entryIterator.hasNext()) {
            Map.Entry<String, User> userEntry = entryIterator.next();
            if (userEntry.getKey().equals(id)) {
                entryIterator.remove();
                break;
            }
        }
    }

    @Override
    public void removeAll() {
        entities.clear();
    }
}

