package ru.trifonov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.trifonov.tm.command.AbstractCommand;
import ru.trifonov.tm.enumerate.RoleType;

public final class ProjectRemoveOneCommand extends AbstractCommand {
    @NotNull
    @Override
    public String getName() {
        return "project-removeOne";
    }

    @NotNull
    @Override
    public String getDescription() {
        return ": removeOne select project";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[REMOVE PROJECT]");
        System.out.println("Enter the ID of the project you want to removeOne");
        @NotNull final String id = serviceLocator.getInCommand().nextLine();
        @NotNull final String userId = serviceLocator.getCurrentUserID();
        serviceLocator.getTaskService().removeAllOfProject(id, userId);
        serviceLocator.getProjectService().removeOne(id, userId);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public RoleType[] roleType() {
        return new RoleType[] {RoleType.REGULAR_USER, RoleType.ADMIN};

    }
}
