package ru.trifonov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.trifonov.tm.command.AbstractCommand;
import ru.trifonov.tm.enumerate.RoleType;

public final class ProjectPersistCommand extends AbstractCommand {
    @NotNull
    @Override
    public String getName() {
        return "project-persist";
    }

    @NotNull
    @Override
    public String getDescription() {
        return ": create new project";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECT PERSIST]");
        System.out.println("Enter name");
        @NotNull final String name = serviceLocator.getInCommand().nextLine();
        @NotNull final String userId = serviceLocator.getCurrentUserID();
        System.out.println("Enter description");
        @NotNull final String description = serviceLocator.getInCommand().nextLine();
        System.out.println("Enter start date. Date format DD.MM.YYYY");
        @NotNull final String beginDate = serviceLocator.getInCommand().nextLine();
        System.out.println("Enter finish date. Date format DD.MM.YYYY");
        @NotNull final String endDate = serviceLocator.getInCommand().nextLine();
        serviceLocator.getProjectService().persist(name, userId, description, beginDate, endDate);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public RoleType[] roleType() {
        return new RoleType[] {RoleType.REGULAR_USER, RoleType.ADMIN};

    }
}
