package ru.trifonov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.trifonov.tm.command.AbstractCommand;
import ru.trifonov.tm.enumerate.RoleType;

public final class ProjectMergeCommand extends AbstractCommand {
    @NotNull
    @Override
    public String getName() {
        return "project-merge";
    }

    @NotNull
    @Override
    public String getDescription() {
        return ": merge select project";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("PROJECT MERGE");
        System.out.println("Enter the ID of the project you want to merge");
        @NotNull final String id = serviceLocator.getInCommand().nextLine();
        @NotNull final String userId = serviceLocator.getCurrentUserID();
        System.out.println("Enter new name");
        @NotNull final String name = serviceLocator.getInCommand().nextLine();
        System.out.println("Enter new description");
        @NotNull final String description = serviceLocator.getInCommand().nextLine();
        System.out.println("Enter new begin date. Date format DD.MM.YYYY");
        @NotNull final String beginDate = serviceLocator.getInCommand().nextLine();
        System.out.println("Enter new end date. Date format DD.MM.YYYY");
        @NotNull final String endDate = serviceLocator.getInCommand().nextLine();
        serviceLocator.getProjectService().merge(name, id, userId, description, beginDate, endDate);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public RoleType[] roleType() {
        return new RoleType[] {RoleType.REGULAR_USER, RoleType.ADMIN};

    }
}
