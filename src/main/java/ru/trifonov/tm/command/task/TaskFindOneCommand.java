package ru.trifonov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.trifonov.tm.command.AbstractCommand;
import ru.trifonov.tm.enumerate.RoleType;

public final class TaskFindOneCommand extends AbstractCommand {
    @NotNull
    @Override
    public String getName() {
        return "task-findOne";
    }

    @NotNull
    @Override
    public String getDescription() {
        return ": return select task";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[FIND TASK]");
        System.out.println("Enter the ID of the task");
        @NotNull final String id = serviceLocator.getInCommand().nextLine();
        @NotNull final String userId = serviceLocator.getCurrentUserID();
        System.out.println(serviceLocator.getTaskService().findOne(id, userId));
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public RoleType[] roleType() {
        return new RoleType[] {RoleType.REGULAR_USER, RoleType.ADMIN};

    }
}
