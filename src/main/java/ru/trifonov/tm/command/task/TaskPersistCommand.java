package ru.trifonov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.trifonov.tm.command.AbstractCommand;
import ru.trifonov.tm.enumerate.RoleType;

public final class TaskPersistCommand extends AbstractCommand {
    @NotNull
    @Override
    public String getName() {
        return "task-persist";
    }

    @NotNull
    @Override
    public String getDescription() {
        return ": create new task";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PERSIST TASK]");
        System.out.println("Enter ID of project");
        @NotNull final String projectId = serviceLocator.getInCommand().nextLine();
        @NotNull final String userId = serviceLocator.getCurrentUserID();
        System.out.println("Enter name task");
        @NotNull final String name = serviceLocator.getInCommand().nextLine();
        System.out.println("Enter description");
        @NotNull final String description = serviceLocator.getInCommand().nextLine();
        System.out.println("Enter start date task. Date format DD.MM.YYYY");
        @NotNull final String beginDate = serviceLocator.getInCommand().nextLine();
        System.out.println("Enter finish date task. Date format DD.MM.YYYY");
        @NotNull final String endDate = serviceLocator.getInCommand().nextLine();
        serviceLocator.getTaskService().persist(name, projectId, userId, description, beginDate, endDate);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public RoleType[] roleType() {
        return new RoleType[] {RoleType.REGULAR_USER, RoleType.ADMIN};
    }
}
