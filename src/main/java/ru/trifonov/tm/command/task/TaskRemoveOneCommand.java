package ru.trifonov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.trifonov.tm.command.AbstractCommand;
import ru.trifonov.tm.enumerate.RoleType;

public final class TaskRemoveOneCommand extends AbstractCommand {
    @NotNull
    @Override
    public String getName() {
        return "task-removeOne";
    }

    @NotNull
    @Override
    public String getDescription() {
        return ": removeOne select task";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[REMOVE TASK]");
        System.out.println("Enter the ID of the task you want to removeOne");
        @NotNull final String id = serviceLocator.getInCommand().nextLine();
        @NotNull final String userId = serviceLocator.getCurrentUserID();
        serviceLocator.getTaskService().remove(id, userId);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public RoleType[] roleType() {
        return new RoleType[] {RoleType.REGULAR_USER, RoleType.ADMIN};
    }
}
