package ru.trifonov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.trifonov.tm.command.AbstractCommand;
import ru.trifonov.tm.enumerate.RoleType;

public final class UserEndOfSessionCommand extends AbstractCommand {
    @NotNull
    @Override
    public String getName() {
        return "user-exit";
    }

    @NotNull
    @Override
    public String getDescription() {
        return ": end of user session";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[USER LOGOUT]");
        serviceLocator.setCurrentUser(null);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public RoleType[] roleType() {
        return new RoleType[] {RoleType.REGULAR_USER, RoleType.ADMIN};
    }
}
