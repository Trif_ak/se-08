package ru.trifonov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.trifonov.tm.command.AbstractCommand;
import ru.trifonov.tm.enumerate.RoleType;

public final class UserFindOneCommand extends AbstractCommand {
    @NotNull
    @Override
    public String getName() {
        return "user-findOne";
    }

    @NotNull
    @Override
    public String getDescription() {
        return ": return select user";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[FIND USER]");
        @NotNull final String id = serviceLocator.getCurrentUserID();
        System.out.println(serviceLocator.getUserService().findOne(id));
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public RoleType[] roleType() {
        return new RoleType[] {RoleType.REGULAR_USER, RoleType.ADMIN};
    }
}
