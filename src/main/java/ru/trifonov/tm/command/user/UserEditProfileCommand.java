package ru.trifonov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.trifonov.tm.command.AbstractCommand;
import ru.trifonov.tm.enumerate.RoleType;

public final class UserEditProfileCommand extends AbstractCommand {
    @NotNull
    @Override
    public String getName() {
        return "user-edit";
    }

    @NotNull
    @Override
    public String getDescription() {
        return ": edit user";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[EDIT REGULAR_USER]");
        @NotNull final String id = serviceLocator.getCurrentUserID();
        System.out.println("Enter new login");
        @NotNull final String login = serviceLocator.getInCommand().nextLine();
        System.out.println("Enter new password");
        @NotNull final String password = serviceLocator.getInCommand().nextLine();
        serviceLocator.getUserService().update(id, login, password, serviceLocator.getCurrentUser().getRoleType());
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public RoleType[] roleType() {
        return new RoleType[] {RoleType.REGULAR_USER, RoleType.ADMIN};
    }
}
