package ru.trifonov.tm.util;

import java.util.UUID;

public class IdUtil {
    public static String getUUID() {
        return UUID.randomUUID().toString();
    }
}
