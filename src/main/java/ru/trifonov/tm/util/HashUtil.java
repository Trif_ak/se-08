package ru.trifonov.tm.util;

import org.jetbrains.annotations.NotNull;

import java.security.MessageDigest;

public class HashUtil {
    public static String getHashMD5(String password) throws Exception{
        @NotNull final byte[] bytesPassword = password.getBytes("UTF-8");
        return new String(MessageDigest.getInstance("MD5").digest(bytesPassword));
    }
}
