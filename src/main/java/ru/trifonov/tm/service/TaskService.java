package ru.trifonov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.api.ITaskRepository;
import ru.trifonov.tm.api.ITaskService;
import ru.trifonov.tm.entity.Task;
import ru.trifonov.tm.util.IdUtil;

import java.util.Collection;

public final class TaskService extends AbstractService implements ITaskService {
    private ITaskRepository taskRepository;

    public TaskService(final ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public void persist(@Nullable final String name, @Nullable final String projectId, @NotNull final String userId, @Nullable final String description, @Nullable final String beginDate, @Nullable final String endDate) throws Exception {
        if (name == null || name.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (projectId == null || projectId.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (description == null || description.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (beginDate == null || beginDate.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (endDate == null || endDate.trim().isEmpty()) throw new NullPointerException("Enter correct data");

        @NotNull final Task task = new Task(IdUtil.getUUID(), projectId, userId, name, description, dateFormat.parse(beginDate), dateFormat.parse(endDate));
        taskRepository.persist(task);
    }

    @Override
    public void merge(@Nullable final String name, @Nullable final String id, @Nullable final String projectId, @NotNull final String userId, @Nullable final String description, @Nullable final String beginDate, @Nullable final String endDate) throws Exception {
        if (name == null || name.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (id == null || id.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (projectId == null || projectId.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (description == null || description.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (beginDate == null || beginDate.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (endDate == null || endDate.trim().isEmpty()) throw new NullPointerException("Enter correct data");

        @NotNull final Task task = new Task(IdUtil.getUUID(), projectId, userId, name, description, dateFormat.parse(beginDate), dateFormat.parse(endDate));
        taskRepository.persist(task);
    }

    @Override
    public Task findOne(@Nullable final String id, @NotNull final String userId) throws Exception {
        if (id == null || id.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        return taskRepository.findOne(id, userId);
    }

    @Override
    public Collection<Task> findAll(@Nullable final String projectId, @NotNull final String userId) throws Exception {
        if (projectId == null || projectId.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        return taskRepository.findAll(projectId, userId);

    }

    @Override
    public void remove(@Nullable final String id, @NotNull final String userId) {
        if (id == null || id.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        taskRepository.remove(id, userId);
    }

    @Override
    public void removeAllOfProject(@Nullable final String projectId, @NotNull final String userId) {
        if (projectId == null || projectId.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        taskRepository.removeAllOfProject(projectId, userId);
    }

    @Override
    public void removeAllOfUser(final String userId) {
        if (userId == null || userId.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        taskRepository.removeAllOfUser(userId);
    }
}