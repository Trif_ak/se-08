package ru.trifonov.tm.service;

import org.jetbrains.annotations.NotNull;

import java.text.SimpleDateFormat;

abstract class AbstractService {
    @NotNull
    final SimpleDateFormat dateFormat = new SimpleDateFormat("DD.MM.YYYY");
}
