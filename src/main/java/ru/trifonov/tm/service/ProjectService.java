package ru.trifonov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.api.IProjectRepository;
import ru.trifonov.tm.api.IProjectService;
import ru.trifonov.tm.entity.Project;
import ru.trifonov.tm.util.IdUtil;

import java.util.Collection;

public final class ProjectService extends AbstractService implements IProjectService {
    private IProjectRepository projectRepository;

    public ProjectService(final IProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public void persist(@Nullable final String name, @NotNull final String userId, @Nullable final String description, @Nullable final String beginDate, @Nullable final String endDate) throws Exception {
        if (name == null || name.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (description == null || description.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (beginDate == null || beginDate.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (endDate == null || endDate.trim().isEmpty()) throw new NullPointerException("Enter correct data");

        @NotNull final Project project = new Project(name, IdUtil.getUUID(), userId, description, dateFormat.parse(beginDate), dateFormat.parse(endDate));
        projectRepository.persist(project);
    }

    @Override
    public void merge(@Nullable final String name, @Nullable final String id, @NotNull final String userId, @Nullable final String description, @Nullable final String beginDate, @Nullable final String endDate) throws Exception {
        if (name == null || name.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (id == null || id.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (description == null || description.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (beginDate == null || beginDate.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (endDate == null || endDate.trim().isEmpty()) throw new NullPointerException("Enter correct data");

        @NotNull final Project project = new Project(name, IdUtil.getUUID(), userId, description, dateFormat.parse(beginDate), dateFormat.parse(endDate));
        projectRepository.merge(project);
    }

    @Override
    public Project findOne(@Nullable final String id, @NotNull final String userId) throws Exception {
        if (id == null || id.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        return projectRepository.findOne(id, userId);
    }

    @Override
    public Collection<Project> findAll(@NotNull final String userId) throws Exception {
        return projectRepository.findAll(userId);
    }

    @Override
    public void removeOne(@Nullable final String id, @NotNull final String userId) {
        if (id == null || id.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        projectRepository.remove(id, userId);
    }

    @Override
    public void removeAll(@NotNull final String userId) {
        projectRepository.removeAll(userId);
    }
}