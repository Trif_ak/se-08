package ru.trifonov.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.trifonov.tm.enumerate.RoleType;

@Setter
@Getter
@NoArgsConstructor
public final class User {
    @NotNull
    private String id;
    @NotNull
    private String login;
    @NotNull
    private String passwordMD5;
    @NotNull
    private RoleType roleType;

    public User(String id, String login, String passwordMD5, RoleType roleType) {
        this.id = id;
        this.login = login;
        this.passwordMD5 = passwordMD5;
        this.roleType = roleType;
    }

    @Override
    public String toString() {
        return " ID " + id +
                "  NAME " + login +
                "  PASSWORD " + passwordMD5 +
                "  USER'S ROLE " + roleType.getRoleType();
    }
}
