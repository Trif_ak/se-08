package ru.trifonov.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import java.util.Date;

@Setter
@Getter
@NoArgsConstructor
public final class Task {
    @NotNull
    private String name;
    @NotNull
    private String id;
    @NotNull
    private String projectId;
    @NotNull
    private String userId;
    @NotNull
    private String description;
    @NotNull
    private Date beginDate;
    @NotNull
    private Date endDate;

    public Task(String name, String id, String projectId, String userId, String description, Date beginDate, Date endDate) {
        this.name = name;
        this.id = id;
        this.projectId = projectId;
        this.userId = userId;
        this.description = description;
        this.beginDate = beginDate;
        this.endDate = endDate;
    }

    @Override
    public String toString() {
        return " ID " + id +
                "  NAME " + name +
                "  DESCRIPTION " + description +
                "  PROJECT START DATE " + beginDate +
                "  PROJECT FINISH DATE " + endDate;
    }
}
