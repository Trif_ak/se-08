package ru.trifonov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import ru.trifonov.tm.entity.Project;


import java.util.Collection;

public interface IProjectService {
    void persist(@Nullable String name, @NotNull String userId, @Nullable String description, @Nullable String beginDate, @Nullable String endDate) throws Exception;
    void merge(@Nullable String name, @Nullable String id, @NotNull String userId, @Nullable String description, @Nullable String beginDate, @Nullable String endDate) throws Exception;
    Project findOne(@Nullable String id, @NotNull String userId) throws Exception;
    Collection<Project> findAll(@NotNull String userId) throws Exception;
    void removeOne(@Nullable String id, @NotNull String userId);
    void removeAll(@NotNull String userId);
}
