package ru.trifonov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.entity.User;
import ru.trifonov.tm.enumerate.RoleType;

import java.util.Collection;

public interface IUserService {
    User authorizationUser(@Nullable String login, @Nullable String password) throws Exception;
    String registrationUser(@Nullable String login, @Nullable String password) throws Exception;
    String registrationAdmin(@Nullable String login, @Nullable String password) throws Exception;
    void update(@NotNull String id, @Nullable String login, @Nullable String password, @Nullable RoleType roleType) throws Exception;
    Collection<User> findAll() throws Exception;
    User findOne(@NotNull String id) throws Exception;
    void removeOne(@NotNull String id);
    void removeAll();
}
