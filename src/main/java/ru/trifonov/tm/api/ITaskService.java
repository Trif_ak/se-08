package ru.trifonov.tm.api;

import ru.trifonov.tm.entity.Task;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;

public interface ITaskService {
    void persist(@Nullable String name, @Nullable String projectId, @NotNull String userId, @Nullable String description, @Nullable String beginDate, @Nullable String endDate) throws Exception;
    void merge(@Nullable String name, @Nullable String id, @Nullable String projectId, @NotNull String userId, @Nullable String description, @Nullable String beginDate, @Nullable String endDate) throws Exception;
    Task findOne(@Nullable String id, @NotNull String userId) throws Exception;
    Collection<Task> findAll(@Nullable String projectId, @NotNull String userId) throws Exception;
    void remove(@Nullable String id, @NotNull String userId);
    void removeAllOfProject(@Nullable String projectId, @NotNull String userId);
    void removeAllOfUser(String userId);
}
