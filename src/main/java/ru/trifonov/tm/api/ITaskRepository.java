package ru.trifonov.tm.api;

import org.jetbrains.annotations.NotNull;
import ru.trifonov.tm.entity.Task;

import java.util.Collection;
import java.util.Date;

public interface ITaskRepository {
    void merge(@NotNull Task task);
    void persist(@NotNull Task task);
    void insert(@NotNull String name, @NotNull String id, @NotNull String projectId, @NotNull String userId, @NotNull String description, @NotNull Date beginDate, @NotNull Date endDate);
    void update(@NotNull String name, @NotNull String id, @NotNull String projectId, @NotNull String userId, @NotNull String description, @NotNull Date beginDate, @NotNull Date endDate);
    Collection<Task> findAll(@NotNull String projectId, @NotNull String userId) throws Exception;
    Task findOne(@NotNull String id, @NotNull String userId) throws Exception;
    void remove(@NotNull String id, @NotNull String userId);
    void removeAllOfProject(@NotNull String projectId, @NotNull String userId);
    void removeAllOfUser(@NotNull String userId);
}
